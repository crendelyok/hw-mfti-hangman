package game
import scala.util.Random

private class Dict {
  val listOfWords: Array[String] = Array("scala", "best", "language")
  def randomWord(): String = {
    Random.shuffle(listOfWords.toList).head
  }
}
