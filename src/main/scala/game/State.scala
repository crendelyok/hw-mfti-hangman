package game

class State(word: String, guessedLetters: Set[Char]) {
  val maxMistakes: Int = math.min(word.length, 5)
  val allGuessedLetters: Set[Char] = guessedLetters
  val wordToGuess: String = word

  def wrongGuesses: Int = (guessedLetters -- word.toSet).size
  def isPlayerLost: Boolean = wrongGuesses >= maxMistakes
  def isPlayerWin: Boolean = allGuessedLetters == word.toSet

  def hideTheWord: String = {
    wordToGuess.map(c => if(guessedLetters.contains(c)) c else '*')
  }
}