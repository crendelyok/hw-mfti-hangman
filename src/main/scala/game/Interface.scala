package game
import scala.io.StdIn

object Interface {
  def loopStart(state: State): Unit = { println(s"The word: ${state.hideTheWord}:\n\nGuess a letter:") }
  def loopHit(state: State): Unit = { println("Hit") }
  def loopMiss(state: State): Unit = { println(s"Missed, mistake ${state.wrongGuesses} out of ${state.maxMistakes}.") }
  def end(state: State): Unit = {
    if (state.isPlayerLost) println("You lost!")
    else println("You won!")
  }
  def read(): Char = StdIn.readChar()
}
