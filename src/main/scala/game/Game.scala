package game

class Game {
  def startGame(): Unit = {
    val dict = new Dict()
    val randomWord = dict.randomWord()
    val state = new State(randomWord, Set())
    gameLoop(randomWord)(state)
  }

  def gameLoop(wordToGuess: String)(state: State): State = {
    val interface = Interface
    if (state.isPlayerLost | state.isPlayerWin) {
      interface.end(state)
      return state
    }

    interface.loopStart(state)

    val newLetter = interface.read()
    val newState = new State(wordToGuess, (state.allGuessedLetters + newLetter))

    if (state.hideTheWord.equals(newState.hideTheWord)) interface.loopMiss(state)
    else interface.loopHit(state)

    gameLoop(wordToGuess)(newState)
  }
}
