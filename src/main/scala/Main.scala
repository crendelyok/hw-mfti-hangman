import game.Game

object Main {
  def main(args: Array[String]): Unit = {
    val game = new Game()
    game.startGame()
  }
}