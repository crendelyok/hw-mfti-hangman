package TestGame
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import game.{State, Game}

class TestGame extends AnyFlatSpec with Matchers {
  trait test1 {
    val game = new Game
    val word = "testscala"
  }

  it should "test1: game should end" in new test1 {
    val state = new State(word, word.toSet)
    game.gameLoop(word)(state) shouldBe state
  }

}
