package TestGame
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import game.State

class TestState extends AnyFlatSpec with Matchers {
  //  test 1
  it should "test1: wrongGuesses is zero" in new State("se", Set[Char]('e')){
    wrongGuesses shouldBe 0
  }

  it should "test1: player did not lost" in new State("se", Set[Char]('e')){
    isPlayerLost shouldBe false
  }

  it should "test1: player did not win" in new State("se", Set[Char]('e')){
    isPlayerWin shouldBe false
  }

  it should "test1: player maxMistakes <= word.length" in new State("se", Set[Char]('e')){
    maxMistakes shouldBe 2
  }

  it should "test1: hidden word is *e" in new State("se", Set[Char]('e')){
    hideTheWord shouldBe "*e"
  }

  // test 2
  it should "test2: wrongGuesses is zero" in new State("helloworld", "helowrd".toSet){
    wrongGuesses shouldBe 0
  }

  it should "test2: player did not lost" in new State("helloworld", "helowrd".toSet){
    isPlayerLost shouldBe false
  }

  it should "test2: player win" in new State("helloworld", "helowrd".toSet){
    isPlayerWin shouldBe true
  }

  it should "test2: player maxMistakes <= word.length" in new State("helloworld", "helowrd".toSet){
    maxMistakes shouldBe 5
  }

  it should "test2: hidden word is 'helloworld" in new State("helloworld", "helowrd".toSet){
    hideTheWord shouldBe "helloworld"
  }

  // test 3
  it should "test3: wrongGuesses is zero" in new State("helloworld", "qwertuiopzxcv".toSet){
    wrongGuesses shouldBe 9
  }

  it should "test3: player lost" in new State("helloworld", "qwertuiopzxcv".toSet){
    isPlayerLost shouldBe true
  }

  it should "test3: player did not win" in new State("helloworld", "qwertuiopzxcv".toSet){
    isPlayerWin shouldBe false
  }

  it should "test3: player maxMistakes <= word.length" in new State("helloworld", "qwertuiopzxcv".toSet){
    maxMistakes shouldBe 5
  }

  it should "test3: hidden word is 'helloworld" in new State("helloworld", "qwertuiopzxcv".toSet){
    hideTheWord shouldBe "*e**owor**"
  }
}
